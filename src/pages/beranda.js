import React from 'react'
import {NavigationBar} from '../components/navbar'
import {Footer} from '../components/footer'
import {FeaturedProgram} from '../components/featuredprogram'

export const Beranda = () => (
    <React.Fragment>
    <NavigationBar />
    <p>Beranda</p>
    <FeaturedProgram />
    <Footer />
    </React.Fragment>

)


